#!/usr/bin/env python

from setuptools import setup, find_packages
import os
import re
import inspect

try:
    long_desc = open('README.md', 'r').read()

except:
    long_desc=''

# get the full path to this file :
thispath = os.path.abspath(os.path.realpath(os.path.split(inspect.stack()[0][1])[0]))

# read version from /pyspectre/__init__.py :
for line in open(os.path.join(thispath, 'circuitgym/__init__.py'), 'r').readlines():                ##_xanity_replace
    scraped = re.search(r'\s*__version__\s*=\s*[\"\'](\S+)[\"\']', line)
    if scraped:
        version = scraped.group(1).strip('\'\"').strip()

setup(
      name='circuitgym',
      version=version,
      description='OpenAI-gym -like environments for spice circuits',
      long_description=long_desc,
      long_description_content_type="text/markdown",
      author='Barry Muldrey',
      author_email='bmuldrey3@gatech.edu',
      url='https://github.gatech.edu/lars/circuitgym',
      packages=find_packages(),
      install_requires=['gym', 'pyspectre>=0.1b10', 'pandas', 'numpy'],
)
